# CMSIS-DAP & Openocd

[TOC]

## Overview

Openocd用來作為CMSIS-DAP的Commander



## IMXRT1xxx NOR flash XIP 調試原理

調試可以分為在SRAM或Flash執行，SRAM調試可以直接將code從JTAG/SWD放入RAM即可，在**Flash調試**，首先**需要Flash下載算法**，讓使用者可以在IDE裡載code到flash並做XIP調試。



通常Cortex-M內核MCU內部會有NOR flash，直接掛在Cortex-M內核AHB總線上，通常IDE如果有支援此款MCU，為了方便使用者使用，一般會同時集成對應的flash下載算法(演算法)，而IMXRT沒有內部flash，而是使用FlexSPI來外掛NOR flash執行XIP。



ARM CoreSight在內核裡主要負責debug工作以及從AMBA總線可以access system memory、周邊的暫存器與外掛flash中的code。

DAP component <-> CoreSight <-> system memory, peripheral registers or code of external serial flash



要執行debug code必須為XIP (CPU可以即時於任何位址去fetch指令與data)，flash要為parallel bus interface且掛在AMBA，但是在spi flash不僅位址與資料線共用且為序列試的，而FlexSPI模組轉換讓我們可以對spi flash做XIP，FlexSPI會自動完成從spi flash讀取指令數據的工作，並將指令存放到AHB Rx buffer裡，CPU直接從AHB Rx buffer讀取指令去執行。



spi flash種類有很多，但大多數會follow JESD216標準，但實際上還是會有差異。flash演算法都要跟IMXRT開發版有關聯，，因為開發版決定了flash連接。啟動IDE調試時，**預先放在IDE裡的flash演算法(可執行檔)會先被載到FlexRAM**，**內容包含FlexSPI初始化、 flash program/erase API**，debuger會將要被燒錄的image分段放在FlexRAM中做燒錄，完成後CPU就可以透過AHB訪問spi flash。

![](images/mcuexpresso-3.jpg)



Flash driver設計概念，iMXRT1050_QSPI.zip有LPCXFlashDriverLib(要先編譯產生libLPCXFlashDriverLib.a)和iMXRT1050_QSPI(生成MIMXRT1050-EVK_IS25WP064A.cfx)，編譯設計與IAR/MDK結構不同，因為NXP考慮到產品線多需要共code的關係。



iMXRT1050_QSPI除了FlexSPI驅動外，還有FlashDev.c和FlashPrg.c，這是CMSIS開源flash算法API定義。算法核心設計在LPCXFlashDriverLib lpcx_flash_memdev.c，定義了重要的MemoryDevice，他會被DTCM(start addr 0x200000000)的前64KB。其中ServiceMessages()是IDE調試器調用flash API的入口。



NXP: cfx其實就是elf可執行檔

Keil C: FLM是由.axf直接改名



## CMSIS-DAP

- CMSIS-DAP (Cortex Microcontroller Software Interface Standard-Debug Access Port)是一種用來連接debug port與USB的介面韌體

  ![](images/cmsis-dap-1.jpg)

- Firmware有兩個版本

  - 版本1配置使用USB HID作做為與PC的接口
  - **版本2**配置使用WinUSB作做為與PC的接口，並提供高速SWO跟蹤流 (目前使用版本)

  

## Openocd

- Openocd (open on-chip debugger)是跑在host PC端，需要調試仿真器(SWD/JTAG)配合使用，再使用gdb client進行調試

  ![](images/openocd-1.jpg)

  

- OpenOCD與GDB之間的溝通是透過TCP/IP，GDB是一種remote serial protocol。Openocd會建立GDB server並等待GDB連接

  ![openocd-2](images/openocd-2.jpg)

  *DTM: debug transport module

- Config files

  - **interface**: debug adapters (cmsis-dap, jlink, stlink...)

  - board: Circuit Board, PWA, PCB

  - **target**: Chip. The “target” directory represents the **JTAG TAPs** on a chip which OpenOCD should control, not a board.

    常見的有ARM chip和FPGA/CPLD，如果一個chip上有多個TAP，則會被定義在target cfg裡。

    (目前openocd官方沒有提供IMXRT系列的cfg file)

    ![](images/openocd_targets.jpg)

    [RT1060 config](https://github.com/sysprogs/openocd/blob/master/tcl/target/imxrt.cfg)

    1. 使用imxrt rom api做寫入FlexSPI
    
    ```
    if { [info exists CHIPNAME] } {
    	set _CHIPNAME $CHIPNAME
    } else {
    	set _CHIPNAME imxrt
    }
    
    source [find mem_helper.tcl]
    
    # ---------------------------------------------- Auxiliary functions for accessing i.MXRT registers ----------------------------------------------
    
    
    # SBMR2: Bit 25..24:
    # BOOT_MODE[1:0]: 00b - Boot From Fuses
    #                 01b - Serial Downloader
    #                 10b - Internal Boot
    #                 11b - Reserved
    proc get_boot_mode {} {
    	set SRC_SBMR2 [ mrw 0x400F801C ]
    	set bootmode [expr ($SRC_SBMR2 & 0x03000000) >> 24 ]
    	return $bootmode
    }
    
    # Boot Device: 0000b - Serial NOR boot via FlexSPI
    #              001xb - SD boot via uSDHC
    #              10xxb - eMMC/MMC boot via uSDHC
    #              01xxb - SLC NAND boot via SEMC
    #              0001b - Parallel NOR boot via SEMC
    #              11xxb - Serial NAND boot via FlexSPI
    proc get_boot_device {} {
    	set SRC_SBMR1 [ mrw 0x400F8004 ]
    	set bootdevice [expr ($SRC_SBMR1 & 0x000000F0) >> 4 ]
    	return $bootdevice
    }
    
    proc get_reset_vector {} {
    	global FLASH_MEMORY_BASE
    	set MAX_FLASH_MEMORY_SIZE 0x10000000
    	
    	set vector_table_addr [ mrw [expr $FLASH_MEMORY_BASE + 0x1004 ] ]
    	if { ($vector_table_addr < $FLASH_MEMORY_BASE) || ($vector_table_addr > ($FLASH_MEMORY_BASE + $MAX_FLASH_MEMORY_SIZE)) } {
    		echo "Invalid vector table address: $vector_table_addr"
    		return 0
    	}
    	
    	set reset_vector [ mrw [expr $vector_table_addr + 4] ]
    	return $reset_vector
    }
    
    # ------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    set RESET_INTO_BOOT_ROM	0
    
    #The regular "reset halt" command on i.MXRT will stop the chip at the internal entry point in the boot ROM.
    #At this point the internal bootloader has not initialized the peripherals set.
    #So normally, we want to instead let the bootloader run and stop when it invokes the entry point of the main program.
    #The 'reset_into_boot_rom' command controls this behavior.
    #Usage: reset_into_boot_rom 0/1
    proc reset_into_boot_rom { flag } {
    	global RESET_INTO_BOOT_ROM
    	set RESET_INTO_BOOT_ROM $flag
    	if { $flag } {
    		echo "'reset halt' will now try to stop in the boot ROM"
    	} else {
    		echo "'reset halt' will now try to stop at the entry point in FLASH"
    	}
    	
    	return ""
    }
    
    set FLASH_MEMORY_BASE 0x60000000
    
    proc init_reset { mode } {
    	global RESET_INTO_BOOT_ROM
    	global PENDING_ENTRY_POINT_ADDRESS
    	set PENDING_ENTRY_POINT_ADDRESS 0
    	
    	if { ($mode eq "run") || $RESET_INTO_BOOT_ROM } {
    		return
    	}
    	
    	halt
    	wait_halt 1000
    	
    	set bootmode [ get_boot_mode ]
    	set bootdev [ get_boot_device ]
    	
    	if { $bootmode != 2 } {
    		echo "Cannot reset into entry when boot mode is $bootmode"
    		return
    	}
    	
    	if { $bootdev != 0 } {
    		echo "Cannot reset into entry when boot device is $bootdev"
    		return
    	}
    	
    	set entry_point [ get_reset_vector ]
    	
    	if { $entry_point == 0 } {
    		echo "Cannot locate the reset vector in FLASH memory. Make sure FLASH is not empty and FlexSPI is initialized."
    		return
    	}
    	
    	set PENDING_ENTRY_POINT_ADDRESS $entry_point
    }
    
    #
    # Only SWD and SPD supported
    #
    source [find target/swj-dp.tcl]
    
    if { [info exists CPUTAPID] } {
    	set _CPU_SWD_TAPID $CPUTAPID
    } else {
    	set _CPU_SWD_TAPID 0x0BD11477
    }
    
    swj_newdap $_CHIPNAME cpu -irlen 4 -expected-id $_CPU_SWD_TAPID
    dap create $_CHIPNAME.dap -chain-position $_CHIPNAME.cpu
    
    set _TARGETNAME $_CHIPNAME.cpu
    target create $_TARGETNAME cortex_m -endian little -dap $_CHIPNAME.dap
    
    if { [info exists WORKAREASIZE] } {
       set _WORKAREASIZE $WORKAREASIZE
    } else {
       set _WORKAREASIZE 0x4000
    }
    
    $_TARGETNAME configure -work-area-phys 0x20200000 \
                           -work-area-size $_WORKAREASIZE \
                           -work-area-backup 0
    					   
    $_TARGETNAME configure -event reset-deassert-post {
    	global PENDING_ENTRY_POINT_ADDRESS
    	set halt_timeout 1000
    	
    	if { $PENDING_ENTRY_POINT_ADDRESS } {
    		wait_halt $halt_timeout
    		
    		set entry_point_hex [ format "0x%X" $PENDING_ENTRY_POINT_ADDRESS ]
    		echo "Found entry point at $entry_point_hex. Setting a temporary breakpoint and resetting..."
    		bp $entry_point_hex 2 hw
    
    		resume		
    		wait_halt $halt_timeout
    		rbp $entry_point_hex		
    	}
    }					   
    
    #Using SRST on i.MXRT devices will not get the chip to halt. Doing a system reset on the ARM Cortex level instead works as expected
    cortex_m reset_config sysresetreq
    reset_config none
    
    #To support FLASH programming on i.MXRT, download the FLASH plugin from https://github.com/sysprogs/flash_drivers and adjust/uncomment the line below:
    #flash bank imxrt plugin $FLASH_MEMORY_BASE 0 0 0 0 flash/IMXRT1050_HyperFLASH_ROMAPI.elf
    ```
    
    

- GDB remote serial protocol



## Openocd flash support

一塊Flash裡面會切成一個或多個"Bank"

一個"Bank"中，裡面還會再切成多個"Sector"

每個Sector中會有自己的大小、Offset(跟Flash起始位置間的偏移)、狀態等等

Flash Driver是提供一系列的函式用來驅動(讀取/寫入/初始化)這個Bank



**src/flash/nor/core.h**

**Bank**

```c
struct flash_bank {
    const char *name;

    struct target *target; /**< Target to which this bank belongs. */

    struct flash_driver *driver; /**< Driver for this bank. */
    void *driver_priv; /**< Private driver storage pointer */

    int bank_number; /**< The 'bank' (or chip number) of this instance. */
    uint32_t base; /**< The base address of this bank */
    uint32_t size; /**< The size of this chip bank, in bytes */

    int chip_width; /**< Width of the chip in bytes (1,2,4 bytes) */
    int bus_width; /**< Maximum bus width, in bytes (1,2,4 bytes) */

    /** Erased value. Defaults to 0xFF. */
    uint8_t erased_value;

    /** Default padded value used, normally this matches the  flash
     * erased value. Defaults to 0xFF. */
    uint8_t default_padded_value;

    /**
     * The number of sectors on this chip.  This value will
     * be set intially to 0, and the flash driver must set this to
     * some non-zero value during "probe()" or "auto_probe()".
     */
    int num_sectors;
    /** Array of sectors, allocated and initialized by the flash driver */
    struct flash_sector *sectors;

    /**
     * The number of protection blocks in this bank. This value
     * is set intially to 0 and sectors are used as protection blocks.
     * Driver probe can set protection blocks array to work with
     * protection granularity different than sector size.
     */
    int num_prot_blocks;
    /** Array of protection blocks, allocated and initilized by the flash driver */
    struct flash_sector *prot_blocks;

    struct flash_bank *next; /**< The next flash bank on this chip */
};
```

- name: 幫這個Bank取名稱，方便之後用name找bank
- driver: 這邊放"Flash Driver"，操作都是從Bank找到Flash Driver後，再到底層去處理
- base/size: 就是放這個Bank的Base Address和大小
- num_sectors: 旗下Sectors的總數
- sectors: 每個Sector都有自己的狀態，這邊用一個Array存放指向這些Sector的指標



**Sector**

```c
struct flash_sector {
    /** Bus offset from start of the flash chip (in bytes). */
    uint32_t offset;
    /** Number of bytes in this flash sector. */
    uint32_t size;
    /**
     * Indication of erasure status: 0 = not erased, 1 = erased,
     * other = unknown.  Set by @c flash_driver_s::erase_check.
     *
     * Flag is not used in protection block
     */
    int is_erased;
    /**
     * Indication of protection status: 0 = unprotected/unlocked,
     * 1 = protected/locked, other = unknown.  Set by
     * @c flash_driver_s::protect_check.
     *
     * This information must be considered stale immediately.
     * A million things could make it stale: power cycle,
     * reset of target, code running on target, etc.
     *
     * If a flash_bank uses an extra array of protection blocks,
     * protection flag is not valid in sector array
     */
    int is_protected;
};
```

- offset: 跟Flash起始位置間的偏移量
- size: 這個Sector的大小 (注意: Sector大小可能不同，請參照文件)
- is_erased: Flash在寫入之前，要先進行Erase，這邊用來記錄這個Sector有沒有被清理過!
- is_protected: 用來保護這個Sector，防止被Erase/Program



## Openocd flash commands

- flash bank [name] [driver] [base addr] [size] [chip width] [bus width] [target] [driver options]

  - name: 給個方便使用的名稱
  - driver: 使用哪套Flash Driver來使用這塊Bank
  - base: Bank的Based Address
  - size: Bank的大小，可以不指定，但要補上0
  - chip_width: 用來標示這個Chip的頻寬，目前大部分的Driver都不需要指定
  - bus_width: 用來標示Data bus的頻寬，目前大部分的Driver都不需要指定
  - target: 用來標示這個Bank屬於哪個Target
  - driver_options: 如果Flash Driver需要其他參數，可以接在後面這邊

- flash write_image [erase] [unlock] [filename] [offset] [type]

  用來燒錄flash

  - erase: 表示在Program這個Flash之前，要先將Sector做Erase
  - unlock: 表示在Program這個Flash之前，要先將Sector做unlock
  - filename: Image的檔名
  - offset: 燒錄的偏移量
  - type: Image的檔案類型，OpenOCD支援以下幾種type
    - bin: Binary二進位檔案
    - elf: ELF檔案

- program [filename] [verify] [reset] [exit] [offset]

  燒錄code後執行指定動作

  - filename: 指定Image的檔案名稱
  - verify: 在燒錄完畢後，是否需要驗證燒錄的資料
  - reset: 燒錄完畢後，將Target進行Reset
  - exit: 燒錄完畢後，直接結束OpenOCD
  - offset: 燒錄的偏移量



## Openocd target cfg (imxrt)

### RT1060 spec

- vector table address offset

  ![](images/imxrt_cfg-1.jpg)



## 藉由visual studio + visualGDB 開發imxrt flash driver

參考文章: [Programming the FLASH memory of i.MXRT devices with OpenOCD](https://visualgdb.com/tutorials/arm/imxrt/flash/)

**openocd plugin flash driver interface -> IMXRT1061_FLASH drive**r

1. visualGDB將MCUXpresso SDK載入，引用sample code中romapi

2. 在project中加入 [common FLASH plugin sources](https://github.com/sysprogs/flash_drivers/tree/master/common) FLASHPluginCommon.cpp和FLASHPluginInterface.h (openocd flash driver interface，plugin藉由FLASHPluginInterface.h call functions)

3. 改寫romapi code，組合成符合openocd flash driver interface的功能

   ```c
   int FLASHPlugin_DoProgramSync(unsigned startOffset, const void *pData, int bytesToWrite)
   {
   	int pages = bytesToWrite / FLASH_PAGE_SIZE;
   	for (int i = 0; i < pages; i++)
   	{
   		status_t status = ROM_FLEXSPI_NorFlash_ProgramPage(FlexSpiInstance,
   			&norConfig,
   			startOffset + i * FLASH_PAGE_SIZE,
   			(const uint32_t *)((const char *)pData + i  * FLASH_PAGE_SIZE));
   		
   		if (status != kStatus_Success)
   			return i * FLASH_PAGE_SIZE;
   	}
   	
   	return pages * FLASH_PAGE_SIZE;
   }
   ```

4. 加入FLASHPluginConfig.h 其中定義 spi flash 的資訊，page size等等

5. build IMRT1061_FLASH 產生elf檔

6. 建立sample code blink_led，選用`IMXRT1061xxxxx_flexspi_nor.ld`，設定debug command

   ```
   -f interface/cmsis-dap.cfg -c "adapter speed 3000" -f target/imxrt.cfg -c "flash bank imxrt plugin $FLASH_MEMORY_BASE 0 0 0 0 $(ProjectDir.forwardslashes)/IMXRT1061_FLASH" -c init
   ```

   ![](images/blinkled_visualgdb_debug_setting.jpg)

   Output (success): 

   注意: 其燒錄位置由0x60002000 app address開始，目前作法是先將0x60000000-0x60002000先藉由MCUXpresso romapi sample code做寫入

   ```
   Open On-Chip Debugger 0.12.0 (2023-02-02) [https://github.com/sysprogs/openocd]
   Licensed under GNU GPL v2
   libusb1 09e75e98b4d9ea7909e8837b7a3f00dda4589dc3
   For bug reports, read
   	http://openocd.org/doc/doxygen/bugs.html
   adapter speed: 3000 kHz
   
   Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
   none separate
   
   Warn : use 'imxrt.cpu' as target identifier, not '0'
   Info : CMSIS-DAP: SWD supported
   Info : CMSIS-DAP: Atomic commands supported
   Info : CMSIS-DAP: Test domain timer supported
   Info : CMSIS-DAP: FW Version = 2.1.0
   Info : CMSIS-DAP: Serial# = 070000011a5a5bb50000000001d36091a5a5a5a597969908
   Info : CMSIS-DAP: Interface Initialised (SWD)
   Info : SWCLK/TCK = 1 SWDIO/TMS = 1 TDI = 0 TDO = 0 nTRST = 0 nRESET = 1
   Info : CMSIS-DAP: Interface ready
   Info : clock speed 3000 kHz
   Info : SWD DPIDR 0x0bd11477
   Info : [imxrt.cpu] Cortex-M7 r1p1 processor detected
   Info : [imxrt.cpu] target has 8 breakpoints, 4 watchpoints
   Info : starting gdb server for imxrt.cpu on 50738
   Info : Listening on port 50738 for gdb connections
   VisualGDB_OpenOCD_Ready
   Info : Listening on port 6666 for tcl connections
   Info : Listening on port 50736 for telnet connections
   Info : accepting 'gdb' connection on tcp/50738
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1828 ms). Workaround: increase "set remotetimeout" in GDB
   Warn : Prefer GDB command "target extended-remote :50738" instead of "target remote :50738"
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1281 ms). Workaround: increase "set remotetimeout" in GDB
   Error executing event gdb-flash-erase-start on target imxrt.cpu:
   wrong # args: should be "expr expression"
   Info : Erasing FLASH: 0x60002000-0x60006000...
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1395 ms). Workaround: increase "set remotetimeout" in GDB
   Info : Programming FLASH (1 sections, 15252 bytes)...
   Info : Programming FLASH section 1/1 (15252 bytes) at 0x60002000...
   Info : flash_write_progress_sync:0x60002000|0x60003000|imxrt
   Info : flash_write_progress_sync:0x60003000|0x60004000|imxrt
   Info : flash_write_progress_sync:0x60004000|0x60005000|imxrt
   Info : flash_write_progress_sync:0x60005000|0x60005b00|imxrt
   Info : flash_write_progress_sync:0x60005b00|0x60005c00|imxrt
   Warn : keep_alive() was not invoked in the 1000 ms timelimit. GDB alive packet not sent! (1871 ms). Workaround: increase "set remotetimeout" in GDB
   Error executing event gdb-flash-write-end on target imxrt.cpu:
   wrong # args: should be "expr expression"
   Info : dropped 'gdb' connection (error -400)
   shutdown command invoked
   Warn : Flash driver of imxrt does not support free_driver_priv()
   ```

   

## Appendix A (JESD216)

JESD216定義了Serial Flash Discoverable Parameters (SFDP)用於NOR flash和NAND flash，描述flash參數與規格的表格。



### How to access SFDP

- `0x5A`: 將SFDF表由flash讀取至主機
- `0x5B`: 將SFDF表由flash copy到 flash緩衝區，適用於Non-Volatile Memory

Serial NOR Flash設備，僅使用`0x5A`即可讀取SFDP表至主機。Flash driver可能不知道flash為SPI或QSPI等模式，因此按照JESD216定義driver應當先使用QSPI探測SFDP是否存在，如果失敗再使用SPI模式。

QSPI 4-4-4 (command, address和data的bus寬度皆為4) -> DSPI -> SPI



注意:

雖然JESD216有定義主機向flash傳送`0x5A`格式應為 1byte命令 + 3 byte地址 + 8個時鐘周期dummy + 數據，但不是所有flash都支援8個時鐘周期dummy，因此flash driver再特測階段會發送不同個數周期的dummy



### SFDP format

SFDP由標題和參數表構成，標題用來描述參數表的版本、類型、大小等，而參數表紀錄具體的參數值，總共為9 DWORDs。



### SFDP header structure

![](images/SFDP_header_struct.jpg)

![](images/SFDP_header_example.jpg)



### JEDEC 參數內容

- 支援1(cmd)-1(addr)-4(data) / 1-4-4 / 1-2-2 / 1-1-2 / 4-4-4 / 2-2-2 fast read (0/1)
- 支援double transfer rate clocking
- address bytes (3 default/4 bytes)
- Block/Sector Erase Sizes (01: 4KB erase)
- flash大小: 如果第31個bit為0 (<2GB)，則後面的數值轉為十進制即為flash size，如果第31個bit為1 (>4GB)，則後面數值轉換為十進制後2^N
- 不同傳輸模式所需的dummy clock個數
- 紀錄Opcode



*DWORD = 32 bits

*number of parameter headers = 0x00 表示1個



### Read SFDP behavior

- SFDP與flash memory address是不會重疊的，且在製造商出廠後不得被更改
- 如果flash有支援reset和hold功能，可以在下讀取SFDP指令過程中被執行
- 未使用到的SFDP空間建議填入0xFF



## Reference

1. [Day 27: 高手不輕易透露的技巧(1/2) - Flash Programming](https://ithelp.ithome.com.tw/articles/10197190)
2. [Day 28: 高手不輕易透露的技巧(2/2) - Flash Driver & Target Burner](https://ithelp.ithome.com.tw/articles/10197309?sc=iThelpR)

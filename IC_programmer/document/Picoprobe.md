# Picoprobe

[TOC]

## 燒錄器功能

1. flash program

2. reset target button (HW)

3. 供電給target board的SW (HW)

4. UART DB9 Tx, Rx, GND pin (使用45MR-1600測試pass)

5. 單步debug (DAPLink with keil c -> pass)

6. Picoprobe燒錄code方法: buttont長按再插入USB，將buttom以jumper取代

7. :exclamation:未來使用者可能透過msys build openocd再使用 (已在部門共用筆電上測試過燒錄)

   - DAPLnik不需要另行安裝driver

8. :exclamation:DAPLink FW code

9. :exclamation: imxrt target cfg 測試




## Picoprobe code 小綠板

1. 對MCU做初始化

2. 對USB做初始化

3. 對CDC (*communications device class*) UART做初始化 (虛擬com port，baud rate = 115200 in `picoprobe_config.h`)

   ```
   // UART config
   #define PICOPROBE_UART_TX 4
   #define PICOPROBE_UART_RX 5
   #define PICOPROBE_UART_INTERFACE uart1
   #define PICOPROBE_UART_BAUDRATE 115200
   ```

4. 對tusb做初始化

5. DAP setup

6. 對led做出初試化 (GPIO25)

7. 開一個thread去執行CDC UART, USB與DAP的任務

   ```
   #define UART_TASK_PRIO (tskIDLE_PRIORITY + 3)
   #define TUD_TASK_PRIO  (tskIDLE_PRIORITY + 2)
   #define DAP_TASK_PRIO  (tskIDLE_PRIORITY + 1) //Lowest priority
   ```

   **CDC task:** 

   當有資料可以讀取且rx_buf未滿時，rx_buf持續讀取UART資料。讀取DTR (bit 0)判斷是否連接上，接著執行Rx與Tx

   *rx_buf最多收256 bytes

   *FIFO writes the received data to rx_buf

   *FIFO reads the transferred data from tx_buf

   **USB task:**

   - RP2040有USB2.0的controller (DP與DM pins, differential pair)，有4kB的DPSRAM用來做為config以及資料的buffer

   - dcd_event_t

     會有FIFO來存放events，藉由while loop來執行所有事件，直到queue中所有事件都被處裡完

      `if ( !osal_queue_receive(_usbd_q, &event, timeout_ms) ) return;`

     ```c
     typedef struct TU_ATTR_ALIGNED(4)
     {
       uint8_t rhport;
       uint8_t event_id;
     
       union
       {
         // BUS RESET
         struct {
           tusb_speed_t speed;
         } bus_reset;
     
         // SOF
         struct {
           uint32_t frame_count;
         }sof;
     
         // SETUP_RECEIVED
         tusb_control_request_t setup_received;
     
         // XFER_COMPLETE
         struct {
           uint8_t  ep_addr;
           uint8_t  result;
           uint32_t len;
         }xfer_complete;
     
         // FUNC_CALL
         struct {
           void (*func) (void*);
           void* param;
         }func_call;
       };
     } dcd_event_t;
     ```

   - DCD_EVENT_BUS_RESET: 設定USB傳輸速度

   - DCD_EVENT_UNPLUGGED: invoke callback (待釐清)

   - DCD_EVENT_SETUP_RECEIVED: Process control request

   - DCD_EVENT_XFER_COMPLETE: 

   - DCD_EVENT_SUSPEND:

   - DCD_EVENT_RESUME:

   - USBD_EVENT_FUNC_CALL:

   - DCD_EVENT_SOF:

   **DAP task:**

   如果FIFO裡有item，則讀取其值至RxDataBuffer -> 經DAP process command -> 傳送出TxDataBuffer

   `DAP_ProcessCommand(RxDataBuffer, TxDataBuffer)`: command request and prepare response

   

## Windows

### Setup env

1. 下載[zadig](https://zadig.akeo.ie/)

2. 執行zadig

   ![](images/zadig_usage.jpg)

3. 安裝driver後，至裝置管理員

   ![](images/device_management.png)

4. 下載[msys2](https://www.msys2.org/)

5. 開啟**MSYS2 MINGW64**，執行以下command

   ```
   # Start by updating the package database and core system packages
   pacman -Syu
   # If MSYS2 closes, start it again
   pacman -Su
   # Install required dependencies
   pacman -S mingw-w64-x86_64-toolchain git make libtool pkg-config autoconf automake texinfo mingw-w64-x86_64-libusb mingw-w64-x86_64-hidapi
   # download openocd package
   git clone https://github.com/raspberrypi/openocd.git --branch rp2040 --depth=1
   cd openocd
   # start to build openocd.exe
   ./bootstrap
   ./configure --disable-werror --enable-cmsis-dap
   make -j4
   ```

6. 測試`openocd.exe`是否正確安裝

   ```
   ./src/openocd.exe
   ```



### Debugging with SWD

#### Experiment1. PC <-> Picoprobe <-> Pico RP2040

##### HW connection

![](images/RaspberryPiPico-AsPicoProbeWiring.jpg)



##### SW debug

Execute `openocd.exe` and DO NOT CLOSE

```
cd openocd
./src/openocd.exe -f interface/cmsis-dap.cfg -c "adapter speed 5000" -f target/rp2040.cfg -s tcl
```

Output:

![](images/openocd_exe_output.jpg)



開啟另一個MSYS2 MINGW64 shell，執行以下command (此範例為燒錄blink.elf)

```
cd  ~/pico-examples/build_temp/blink
gdb-multiarch.exe blink.elf
target remote localhost:3333
load
monitor reset init
continue
```

Output:

After executing `load`

```
Loading section .boot2, size 0x100 lma 0x10000000
Loading section .text, size 0x1e80 lma 0x10000100
Loading section .rodata, size 0xf4 lma 0x10001f80
Loading section .binary_info, size 0x20 lma 0x10002074
Loading section .data, size 0x18c lma 0x10002094
Start address 0x100001e8, load size 8736
Transfer rate: 5 KB/sec, 1747 bytes/write.
```

After executing `monitor reset init`

```
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x000000ea msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x000000ea msp: 0x20041f00
```

finish...



##### SW flash program

範例為執行pico-examples/build_temp/hello_world/serial/hello_serial.bin

```
cd openocd
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/rp2040.cfg -c "adapter speed 5000" -s tcl -c "program ../pico-examples/build_temp/hello_world/serial/hello_serial.bin exit 0x10000000"  
```

Output:

```
Open On-Chip Debugger 0.11.0-g8e3c38f (2023-03-16-08:57)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
Info : Hardware thread awareness created
Info : Hardware thread awareness created
Info : RP2040 Flash Bank Command
adapter speed: 5000 kHz

Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E660B440076CB226
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 5000 kHz
Info : SWD DPIDR 0x0bc12477
Info : SWD DLPIDR 0x00000001
Info : SWD DPIDR 0x0bc12477
Info : SWD DLPIDR 0x10000001
Info : rp2040.core0: hardware has 4 breakpoints, 2 watchpoints
Info : rp2040.core1: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for rp2040.core0 on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x000000ea msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x000000ea msp: 0x20041f00
** Programming Started **
Info : RP2040 B0 Flash Probe: 2097152 bytes @10000000, in 512 sectors

target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
Info : Writing 12288 bytes starting at 0x0
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x00000184 msp: 0x20041f00
** Programming Finished **
shutdown command invoked
```

重新上電，開啟`putty` com port確認輸出

![](images/openocd_program_flash.jpg)



### Experiment2. PC <-> Picoprobe <-> LPC824 (45MR-1600)

##### lpc8xx.cfg

- WORKAREASIZE: 設置flash燒錄過程中可以使用的RAM size
- call lpc1xxx.cfg



##### lpc1xxx.cfg

- CHIPNAME = lpc8xx
- CHIPSERIES = lpc800
- CCLK (CPU core clock freq) = 12Mhz (default)
  - 45MR core freq為30Mhz，實驗結果一樣



##### SW flash program with default setting (fail)

```
cd openocd
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/lpc8xx.cfg -s tcl -c "program C:/Users/MeichiMC_Wang/Desktop/red_led.bin exit 0x00000000"
```

**使用openod default target config檔輸出結果**

- Warn: boot過程中MCU會計算向量表的checksum與image裡的checksum做比對，兩者計算結果不同，但是openocd會寫入正確的值
  - 拿掉checksum功能
- Error: 沒有足夠的working area (RAM)做燒錄動作 (default為1KB)
  - 加大RAM size至2KB

```
Open On-Chip Debugger 0.11.0-g8e3c38f (2023-03-16-08:57)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
adapter speed: 5000 kHz

Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
cortex_m reset_config sysresetreq

Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E660B440076CB226
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 10 kHz
Info : SWD DPIDR 0x0bc11477
Info : lpc8xx.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for lpc8xx.cpu on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x1fff0008 msp: 0x10000ffc
** Programming Started **
Warn : Boot verification checksum in image (0x00000000) to be written to flash is different from calculated vector checksum (0xefffe63d).
Warn : OpenOCD will write the correct checksum. To remove this warning modify build tools on developer PC to inject correct LPC vector checksum.
Warn : not enough working area available(requested 1024)
Error: no working area specified, can't write LPC2000 internal flash
Error: error writing to flash at address 0x00000000 at offset 0x00000000
** Programming Failed **
shutdown command invoked
```



##### SW flash program (fail -> success)

```
./src/openocd.exe -f interface/cmsis-dap.cfg -f target/lpc8xx.cfg -s tcl -c "program C:/Users/MeichiMC_Wang/Desktop/red_led.bin verify reset exit 0x00000000" -c "adapter speed 5000"
```

**使用更改過的openod default target config檔輸出結果**

- Warn: boot過程中MCU會計算向量表的checksum與image裡的checksum做比對，兩者計算結果不同，但是openocd會寫入正確的值
  - 拿掉checksum功能 -> **:exclamation:checksum功能不可省略**
- Error: 沒有足夠的working area (RAM)做燒錄動作 (default為1KB)
  - 加大RAM size至2KB
- 45MR led沒亮

 燒錄過程中沒有比對checksum功能output (fail):

```
Open On-Chip Debugger 0.11.0-g8e3c38f (2023-03-16-08:57)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
adapter speed: 5000 kHz

Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
cortex_m reset_config sysresetreq

Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E660B440076CB226
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 10 kHz
Info : SWD DPIDR 0x0bc11477
Info : lpc8xx.cpu: hardware has 4 breakpoints, 2 watchpoints
Info : starting gdb server for lpc8xx.cpu on 3333
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0xf1000000 pc: 0x1fff0008 msp: 0x10000ffc
** Programming Started **
** Programming Finished **
** Verify Started **
** Verified OK **
** Resetting Target **
shutdown command invoked
```

加大RAM設置+燒錄過程比對checksum(success):

```
** Programming Started **
Warn : Boot verification checksum in image (0x00000000) to be written to flash is different from calculated vector
checksum (0xefffe63d).
Warn : OpenOCD will write the correct checksum. To remove this warning modify build tools on developer PC to inject
 correct LPC vector checksum.
** Programming Finished **
** Verify Started **
Error: checksum mismatch - attempting binary compare
** Verify Failed **
shutdown command invoked
```



## Appendix A (Raspberry Pi & VMware Ubuntu)

### Quick Pico Setup

Download and run `pico_setup.sh`

The script will:

- Create a directory called `pico`
- Install required dependencies
- Download the `pico-sdk`, `pico-examples`, `pico-extras`, and `pico-playground` repositories
- Define `PICO_SDK_PATH`, `PICO_EXAMPLES_PATH`, `PICO_EXTRAS_PATH`, and `PICO_PLAYGROUND_PATH` in your `~/.bashrc`
- Build the `blink` and `hello_world` examples in `pico-examples/build/blink` and `pico-examples/build/hello_world`
- Download and build `picotool`, and copy it to `/usr/local/bin`.
- Download and build `picoprobe`.
- Download and compile **OpenOCD (for debug support)**
- Download and install Visual Studio Code
- Install the required Visual Studio Code extensions
- Configure the Raspberry Pi UART for use with Raspberry Pi Pico

```
mkdir RaspberryPiPico
cd RaspberryPiPico
sudo snap install --classic code
wget https://raw.githubusercontent.com/raspberrypi/pico-setup/master/pico_setup.sh
chmod +x pico_setup.sh
./pico_setup.sh
```



### Install the Toolchain

需要先安裝build code所需要的工具，CMake、cross-platform tool以及GNU Embedded Toolchain for Arm

```
sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential
sudo apt install libstdc++-arm-none-eabi-newlib
```

Note:

Ubuntu and Debian users might additionally need to also install libstdc++-arm-none-eabi-newlib



### Blinking an LED in C

We’re going to go ahead and blink the on-board LED on the Raspberry Pi Pico which is connected to **pin 25** of the **RP2040**



**Building "Blink"**

Set the `PICO_SDK_PATH`:

```
export PICO_SDK_PATH=../../pico-sdk
echo $PICO_SDK_PATH
```

Prepare your cmake build directory by running cmake ..

```
cmake ..
```

Note:

cmake will default to a `Release` build with compiler optimisations enabled and debugging information removed. To build a debug version, run `cmake -DCMAKE_BUILD_TYPE=Debug ..`

:exclamation: Important (燒錄板設定):

The SDK will build the binary targetting the Raspberry Pi Pico by default. If you are building a binary for a different board you need to pass `-DPICO_BOARD=board` where `board` is the name of the board. For instance if you’re building a binary for Pico W you should pass, `-DPICO_BOARD=pico_w` along with information about your wireless network `-DWIFI_SSID="Your Network"` `-DWIFI_PASSWORD="Your Password"`. 

CMake has now prepared a build area for the pico-examples tree.

Build blink code:

```
cd blink
make
```

Output:

```
[  0%] Performing build step for 'ELF2UF2Build'
[100%] Built target elf2uf2
[  0%] No install step for 'ELF2UF2Build'
[  0%] Completed 'ELF2UF2Build'
[  0%] Built target ELF2UF2Build
Scanning dependencies of target bs2_default
[  0%] Built target bs2_default
[  0%] Built target bs2_default_padded_checksummed_asm
Scanning dependencies of target blink
Consolidate compiler generated dependencies of target blink
[100%] Built target blink
```

• `blink.elf`, which is used by the debugger
• `blink.uf2`, which can be dragged onto the RP2040 USB Mass Storage Device



### 燒錄方法

環境於windows10下操作

1. 未上電狀態下，按住開發板上的button
2. 插上USB至PC端，即可看到Pico的磁碟
3. 將uf2檔拖拉至磁碟裡，燒錄完成



### Flash Programming with SWD

SWD (serial wire debug) 是Cortex-M-based 處裡器的標準接口

Build OpenOCD on Linux:

```
sudo apt-get install libhidapi-dev
sudo apt install automake autoconf build-essential texinfo libtool libftdi-dev libusb-1.0-0-
dev
cd openocd
./bootstrap
./configure --enable-cmsis-dap --enable-sysfsgpio --enable-bcm2835gpio
make
sudo make install
```

Output:

![](images/openocd_config.png)

These build instructions assume you are running on Linux, and have installed the SDK:

```
cd picoprobe
mkdir build_temp
cd build_temp
git submodule update --init
cmake ..
make
```



## Appendix B (SPI flash indrect programming)

J-Link接到MCU debug接口，將code載到RAM裡再搬至spi flash，而target interface通常為JTAG或SWD。

注意:

- J-Link (openocd) 需要知道支持SPI flash的CPU架構
- A special programming algorithm is necessary per design (different SPI units may be used. SPI controller of MCU 0 is different to MCU 1, ...)



## Appendix C (TCL語法)

- `source [find mem_helper.tcl]` source用來打開文件
- `proc`用來定義新的函數

```
proc get_boot_mode {} {
	set SRC_SBMR2 [ mrw 0x400F801C ]
	set bootmode [expr ($SRC_SBMR2 & 0x03000000) >> 24 ]
	return $bootmode
}
```

- `expr`命令是Tcl裡專門用來做運算的命令，一般的情況我們透過expr及子命令代換來達成運算的需求

## Action item

1. 開發板測試

   - 燒錄
   - 單步執行

   ![](images/RaspberryPiPico-1.jpg)

2. 開發版搭配LPC824測試

   - 燒錄

   - 單步執行

     ![](images/RaspberryPiPico-2.jpg)
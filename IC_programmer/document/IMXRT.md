# IMXRT

## IMXRT1060 外掛SDRAM

RT1062有1MB的OCRAM，其中512KB為tightly coupled memory, TCM，對於一般應用已足夠，但當不夠使用時可以透過smart external memory controller, SEMC做擴充RAM，SEMC是一種支援多個標準、並行的儲存控制器，最高工作頻率為166Mhz，可以應用在DRAM、NOR Flash、SRAM、NAND Flash等，對於SDRAM，bus支援8-bit與16-bit兩種模式。

PCB layout時需要注意佈線(信號線等長處理)，差度差<50mm、阻抗匹配等 (使用不同SDRAM會有所差異)。



![](images/IMXRT_SDRAM-1.jpg)



SEMC的clock source可以為PLL2(系统PLL，528MHz)的PFD2或者PLL3(USB1 PLL，480MHz)的PFD1，也可以直接選擇PERIPH CLK。PFD(Phase Fractional Dividers)是 System PLL和USB1 PLL的输出clock，PFD頻率計算：FVCO * 18 / N，其中 N 的取值範圍：12~35，假設要配置166Mhz，source clock選擇PLL2的PFD2:

![](images/IMXRT_SDRAM-2.jpg)



SDRAM參數配置需要參考SDRAM datasheet才會知道，而具體code的設置可以參考SDK -> driver_examples -> SEMC 範例裡的 BOARD_InitSEMC() 函數。



RT1060可以藉由AXI和IP bus來access SDRAM，AXI主要用來讀寫data，code中需要先定義一個pointer指向SDRAM (0x80000000)。IP bus除了讀寫還可以做控制操作，SDRAM的IP cmd包括: READ（0X8）、WRITE（0X9）、MODESET（0XA）、ACTIVE（0XB）、AUTO REFRESH（0XC）、SELF REFRESH（0XD）、PRECHARGE（0XE）、PRECHARGE ALL（0XF）。



## 如何在flexspi nor flash模式下調試

### XIP啟動流程

BootRom根據BOOT_MODE和eFuse狀態決定使用哪一個啟動設備，使用 eFUSEs決定啟動設備可能會被 GPIO 輸入引腳所覆蓋。

![](images/XIP啟動設備SW.jpg)



![](images/flexspi_nor啟動流程.jpg)



FlexSPI NOR Flash可執行文件由以下構成:

- flash配置參數: 讀取命令序列、flexspi頻率、quad模式
- IVT: 讓ROM知道可執行檔個component位址
- 啟動數據: 用來指定可執行檔的位置 (boot_data)
- DCD: IC配置數據 (例如: SDRAM 配置，refer to SDK中的dcd_data[])



![](images/destination_memory.jpg)

**MIMXRT1052xxxxx_flexspi_nor.icf**可以看到配置參數、IVT、啟動數據、DCD數據在flash中的布局。

![](images/啟動文件位置訊息.jpg)







## Reference

1. [RT1060 外挂 SDRAM](https://www.wpgdadatong.com/cn/blog/detail/43530)
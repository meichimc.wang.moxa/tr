# DAPLink

[TOC]

## Overview

- DAPLink提供三種介面

  - drag-and-drop programming (拖拉燒錄code到target MCU): 完成燒錄後重新上電，如果失敗會出現`FAIL.TXT`，支援的檔案格式有bin和hex。此方法target的flash algorithm必須build在DAPLink FW中[link](https://github.com/ARMmbed/DAPLink/blob/main/docs/PORT_TARGET_FAMILY.md)。
  - serial port: 支援雙向通訊，傳送break command可以reset target MCU
  - 支援debug: 支援有CMSIS-DAP的IDE做debug
    - [pyOCD](https://github.com/mbedmicro/pyOCD)
    - [uVision](http://www.keil.com/)
    - [IAR](https://www.iar.com/)

- 小綠板 FW upgrade

  在插入USB前hold住reset button，此時小綠板進入bootloader mode，複製FW到磁碟裡再新上電，若更新失敗bootloader會出現`FAIL.TXT`

- DAPLink build output

  - `*_if.bin`: 此為base file不會被使用到
  - `*_if_crc.bin`: 燒錄在新的板子上，但是不能與舊版的BL混合使用，考慮到vector table的驗證
  - `*_if_crc.hex`: 與bin內容相同
  - `*_if_crc_legacy_0x5000.bin`: 此image被使用在已經有BL的板子，AP code從0x5000開始燒錄。此檔案式在`*_if_crc_legacy_0x8000.bin`前pending 0x3000 bytes，前0x40 bytes式複製vector table，其餘的則是填充0xFF，如此一來不需要重新build code，也可以將bin檔燒在05000的位址
  - `*_if_crc_legacy_0x8000.bin`: 此image被使用在已經有BL的板子，AP code從0x8000開始燒錄
  -  `tools/post_compute_crc.py`: 此檔案內容使用output image來產生以上檔案格式

- Image structure

  - `post_compute_crc.py` 將checksum(前7個vector會被加總 offset 0x00-0x18，值會被存放在 offset 0x1C，在特定NXP MCU中會做比對，而其他MCU則會忽略)與CRC值( base output file `*_if.bin`的CRC32值，存放在image的最後4 bytes )放入image裡

    ![](images/stm32f103xb_bl.jpg)

    ![stm32f103xb_bl_crc](images/stm32f103xb_bl_crc.jpg)

  - Vector table被定義在 `startup_MCUNAME.S` 裡

- Build information fields

  這些資訊會被放在vector table的起始位址offset 0x20，refer to ` daplink.h` 

  - `DAPLINK_BUILD_KEY`: 分辨image為bootloader(0x9B939D93)或interface(0x9B939E8F)
  - `DAPLINK_HIC_ID`: 定義使用小綠板的MCU型號，確保不會被誤燒到不同型號的MCU
  - `DAPLINK_VERSION`: 軟體版本，原本是用來檢查BL和AP軟體兼容性，現在拿掉這個功能，但可以在`details.txt`裡查看。這個改變會對一些舊版BL產生影響，因此從0x20開始的12 bytes改寫為0x00，並且重新計算checksum值。可以比對`*_if_crc.bin` 與 `*_if_crc_legacy_0x8000.bin` 
  
  ![](images/stm32f103xb_bl_info.jpg)



## Build DAPLink firmware

Run on Ubuntu 22.01.1



### Setup

- Install Python 3
- Install Git
- Install a compiler
  - [GNU Arm Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) . This compiler will be identified as `gcc_arm`.
  - [Arm Compiler 6](https://developer.arm.com/tools-and-software/embedded/arm-compiler) . This compiler will be identified as `armclang`. Only supported on Linux and Windows.
  - [Keil MDK](https://developer.arm.com/tools-and-software/embedded/keil-mdk) or [Arm Compiler 5](https://developer.arm.com/tools-and-software/embedded/arm-compiler/downloads/legacy-compilers#arm-compiler-5). This compiler will be identified as `armcc`. Only supported on Linux and Windows.
- Install `make` (tested with [GNU Make](https://www.gnu.org/software/make)). [CMake](https://cmake.org/) can alternatively be used in conjunction with different implementations of `make` as well as [ninja](https://ninja-build.org/).
- Install virtualenv in your global Python installation eg: `pip install virtualenv`.



Get the sources and create a virtual environment

```
git clone https://github.com/mbedmicro/DAPLink
cd DAPLink
pip install virtualenv
virtualenv --version
virtualenv venv
```

Output:

```
created virtual environment CPython3.9.13.final.0-64 in 4113ms
  creator CPython3Windows(dest=C:\Users\MeichiMC_Wang\Documents\TR\tr\IC_programmer\daplink\DAPLink\venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=C:\Users\MeichiMC_Wang\AppData\Local\pypa\virtualenv)
    added seed packages: pip==23.0.1, setuptools==67.4.0, wheel==0.38.4
  activators BashActivator,BatchActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator
```



### Activate virtual environment (start from here)

Activate the virtual environment and update requirements. This is necessary when you open a new shell. **This should be done every time you pull new changes**

```
venv/Scripts/activate   (For Linux)
cd venv/Scripts			(For Windows)
activate.bat   			(For Windows)
cd ../../
(venv) $ pip install -r requirements.txt //MUST TO DO
```



### Build

[DAPLink Developers Guide link](https://github.com/ARMmbed/DAPLink/blob/main/docs/DEVELOPERS-GUIDE.md)

```
python3 tools/progen_compile.py -t make_gcc_arm --clean -v stm32f103xb_bl
```

- `-t <tool>`: choose the toolchain to build. The default is `make_gcc_arm`. Other options tested are `make_gcc_arm`, `make_armclang`, `make_armcc`, `cmake_gcc_arm`, `cmake_armclang`, `cmake_armcc`.
- `--clean`: will clear existing compilation products and force recompilation of all files.
- `-v`: will make compilation process more verbose (typically listing all commands with their arguments)
- `--parallel`: enable parallel compilation within a project (projects are compiled sequentially).
- `<project>`: target project to compile (e.g. `stm32f103xb_bl`, `lpc11u35_if`), if none is specified all (140 to 150) projects will be compiled.



產生uvision projects

```
progen generate -t uvision
```



## Hardware (stm32f103xb)

**Spec**

- Cortex-M3 72 Mhz
- 128 KB Flash
- 20 KB RAM



**Memory map**

| Region | Size   | Start       | End         |
| ------ | ------ | ----------- | ----------- |
| Flash  | 128 KB | 0x0800_0000 | 0x0802_0000 |
| SRAM   | 20 KB  | 0x2000_0000 | 0x2000_5000 |



**DAPLink default pin assignment**

| **Signal**      | **I/O** | **Symbol** | **Pin** |
| --------------- | ------- | ---------- | ------- |
| SWD / JTAG      |         |            |         |
| **SWCLK / TCK** | **O**   | **PB13**   | **26**  |
| **SWDIO / TMS** | **O**   | **PB14**   | **27**  |
| **SWDIO / TMS** | **I**   | **PB12**   | **25**  |
| SWO / TDO       | I       | PA10       | 31      |
| nRESET          | O       | PB0        | 18      |
| UART            |         |            |         |
| UART RX         | I       | PA2        | 12      |
| UART TX         | O       | PA3        | 13      |
| Button          |         |            |         |
| NF-RST But.     | I       | PB15       | 28      |
| LEDs            |         |            |         |
| Connect. LED    | O       | PB6        | 42      |
| HID LED         | O       | PA9        | 30      |
| CDC LED         | O       | PA9        | 30      |
| MSC LED         | O       | PA9        | 30      |



**Schematic**

![](images\daplink-stmf103cbt6-sch.jpg)



## Appendix

### stm32f103rbt6 開發板 10 pin connector

**![](images/stm32f103rbt6-20pin-connector.jpg)**



### stm32f103rbt6 開發板硬體接線注意事項

**PB12(SWDIO_IN)與PB14(SWDIO_OUT)必須對接**

**![](images/str32f103rbt6硬體注意區塊.jpg)**



### AP code size 判斷

#### Design

![](images/stm32f103xB_memory_layout.jpg)

![](images/stm32f103xB_check_AP_size.jpg)



#### Result

:exclamation: 目前若燒錄的code大於79KB，會跳出FAIL.txt，但是code仍會被燒錄至flash (skip erase chip)。再次插拔燒錄79KB的AP，錯誤燒錄之內容會被覆蓋。

![](images/stm32f103xB_check_AP_size_result.jpg)



#### Note

預期: 產生BL虛擬磁碟後，拉AP code進去會產生fail.txt

實際: maintance沒有fail.txt，也有將AP code燒錄且跳址過去

```c
vfs_user_build_filesystem()
	// FAIL.TXT
    if (vfs_mngr_get_transfer_status() != ERROR_SUCCESS) {
        file_size = get_file_size(read_file_fail_txt);
        vfs_create_file("FAIL    TXT", read_file_fail_txt, 0, file_size);
    }

// drag-n-drop\iap_flash_intf.c
static error_t intercept_page_write(uint32_t addr, const uint8_t *buf, uint32_t size)
 	// meichi test: ERROR_AP_CODE_SIZE
    if (size > 0x00000C00) { //DAPLINK_ROM_UPDATE_SIZE
        return ERROR_AP_CODE_SIZE;
    }
```



強制在intercept_page_write fail return，以確認是否有執行到intercept_page_write fumction

測試結果: 有進入，且fail.txt也有正確輸出

```c
// drag-n-drop\iap_flash_intf.c
static error_t intercept_page_write(uint32_t addr, const uint8_t *buf, uint32_t size)
    // meichi test: ERROR_AP_CODE_SIZE
    if (meichi_test) { //DAPLINK_ROM_UPDATE_SIZE
        return ERROR_AP_CODE_SIZE;
    } 
```
